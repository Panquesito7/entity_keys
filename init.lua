------------------------------------------------------------
-- Copyleft (Я) 2023 mazes
-- https://gitlab.com/mazes_80/entity_keys
------------------------------------------------------------

local init_time = os.clock()

local S = minetest.get_translator("entity_keys")
local SK = minetest.get_translator("keys")

entity_keys = {}

local skeleton_key_on_use = minetest.registered_craftitems["keys:skeleton_key"].on_use

local entity_keys_skeleton_key_on_use = function(itemstack, user, pointed_thing)
	if pointed_thing.type == "node" then
		return skeleton_key_on_use(itemstack, user, pointed_thing)
	end

	if pointed_thing.type ~= "object" then
		return itemstack
	end

	local luaentity = pointed_thing.ref:get_luaentity()
	if not luaentity -- This entity is "alive"
		or not luaentity.on_skeleton_key_use -- It knows this API
		or luaentity.owner ~= user:get_player_name() -- User owns the entity
	then
		return itemstack
	end

	-- make a new key secret in case the node callback needs it
	local random = math.random
	local newsecret = string.format(
		"%s-%04x%04x%04x%04x", luaentity.name,
		random(2^16) - 1, random(2^16) - 1,
		random(2^16) - 1, random(2^16) - 1)

	local secret, description = luaentity:on_skeleton_key_use(user, newsecret)

	if secret then
		local inv = minetest.get_inventory({type="player", name=user:get_player_name()})

		-- update original itemstack
		itemstack:take_item()

		-- finish and return the new key
		local new_stack = ItemStack("keys:key")
		local meta = new_stack:get_meta()
		meta:set_string("secret", secret)
		if not description then
			description = pointed_thing.ref.name:gsub(":", " ")
		end
		meta:set_string("description", SK("Key to @1's @2", user:get_player_name(),
			description))

		if itemstack:get_count() == 0 then
			itemstack = new_stack
		else
			if inv:add_item("main", new_stack):get_count() > 0 then
				minetest.add_item(user:get_pos(), new_stack)
			end -- else: added to inventory successfully
		end

		return itemstack
	end
end

minetest.override_item("keys:skeleton_key", { on_use = entity_keys_skeleton_key_on_use })

local entity_keys_key_on_secondary_use = function(itemstack, placer, pointed_thing)
	if pointed_thing.type ~= "object" then
		return nil
	end -- quit if none is pointed

	local luaentity = pointed_thing.ref:get_luaentity()
	if not luaentity then
		return nil
	end -- quit if not an "alive" entity

	if not placer or not placer:is_player()
		or placer:get_player_control().sneak
		or not luaentity.on_key_use then
		if luaentity.on_rightclick then
			return luaentity:on_rightclick(placer)
		end -- Relay click to entity
		return nil
	end -- Quit if can't relay click

	local meta = itemstack:get_meta()
	local secret = meta:get_string("secret")
	if secret == luaentity.key_lock_secret then
		luaentity:on_key_use(placer)
	end
end

minetest.override_item("keys:key", { on_secondary_use = entity_keys_key_on_secondary_use })

entity_keys.on_skeleton_key_use = function(entity, player, newsecret)
	local pn = player:get_player_name()
	local desc = entity.description or entity.name:gsub(":", " ")

	-- verify placer is owner of lockable chest
	if entity.owner ~= pn then
		minetest.chat_send_player(pn, S("You do not own this: ") .. desc)
		return nil
	end

	if  not entity.key_lock_secret
		or entity.key_lock_secret == "" then
		entity.key_lock_secret = newsecret
	end

	return entity.key_lock_secret, desc
end

entity_keys.can_use_key = function (entity, player)
	if entity.owner == player:get_player_name() then
		return true
	end
	if not entity.key_lock_secret then
		return false
	end
	local wielditem = player:get_wielded_item()
	local itemname = wielditem:get_name()
	if itemname == "keys:key" then
		local key_meta = wielditem:get_meta()
		if key_meta:get_string("secret") == entity.key_lock_secret then
			return true
		end
	elseif minetest.get_item_group(itemname, "key_container") > 0 and
		keyring.fields.utils.KRS.in_serialized_keyring(wielditem, entity.key_lock_secret) then
		return true
	end
	return false
end

print(string.format("[MOD] %s loaded in %.4fs", "entity_keys", os.clock() - init_time))
