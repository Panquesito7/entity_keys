max_line_length = false
quiet = 1

globals = {
	"entity_keys",
}

read_globals = {
	-- Stdlib
	string = {fields = {"split"}},
	table = {fields = {"copy", "getn", "insert_all"}},

	"minetest", "ItemStack"
}
